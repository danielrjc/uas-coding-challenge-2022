const ptRow = require('./pascal');

test('1st row', () => {
    expect(ptRow(0)).toEqual([1]);
});

test('2nd row', () => {
    expect(ptRow(1)).toEqual([1,1]);
});
test('3rd row', () => {
    expect(ptRow(2)).toEqual([1,2,1]);
});
test('8th row', () => {
    expect(ptRow(7)).toEqual([1,7,21,35,35,21,7,1]);
});
test('non valid number', () => {
    expect(ptRow(-5)).toEqual([]);
});