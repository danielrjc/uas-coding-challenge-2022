const ptRow = (n) =>
{
    /* your code here */
    if(n<0){
        return []
    }
    let row = [1]

    //from binomial theorem nCr/nCr-1 = n-r+1/r nCr-1= row[i-1]
    for(let i=1;i<n+1;i++){
        row.push((n-i+1)*row[i-1]/i)
    }
    return row
}

module.exports = ptRow;